package com.joaodjunior.model.users;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.EmailAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.auth.UserInfo;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.joaodjunior.model.Usuario;

import android.content.Intent;
import android.net.Uri;
import android.util.Log;


public class UserDAO {
	
	private static final String TAG = "UserDAO";
	
	private FirebaseAuth autenticacao;
	
	private FirebaseAuth.AuthStateListener autenticacaoListener;
	
	private FirebaseUser usuarioFirebase;
		
	public UserDAO() {
		super();
	}

	public void signUp(Usuario usuario) {
		autenticacao.createUserWithEmailAndPassword(usuario.getEmail(), usuario.getPswd())
					.addOnCompleteListener(new OnCompleteListener<AuthResult>() {
			@Override
			public void onComplete(Task<AuthResult> task) {
				if(!task.isSuccessful()) {
					//Toast.makeText(UserDAO.this, te,Toast.LENGTH_SHORT).show();
					System.out.println("Passed");
				}
				
			}
			
		});
	}
	
	public void signIn(Usuario usuario) {
		autenticacao.signInWithEmailAndPassword(usuario.getEmail(), usuario.getPswd())
					.addOnCompleteListener(new OnCompleteListener<AuthResult>() {

						@Override
						public void onComplete(Task<AuthResult> task) {
							
							if(!task.isSuccessful()) {
								System.out.println("Passed");
								
								usuarioFirebase = FirebaseAuth.getInstance().getCurrentUser();
								if(usuarioFirebase != null) {
									
									//dados importantes do usuario
									System.out.println(usuarioFirebase.toString());
									
								}
							}
							
						}
					});
	}
	
	public void signInWithGoogle() {
		GoogleSignInOptions options = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
				.requestIdToken("")
				.requestEmail()
				.build();
	}
	
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
                // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == 1) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            if (result.isSuccess()) {
                // Google Sign In was successful, authenticate with Firebase
                GoogleSignInAccount account = result.getSignInAccount();
                firebaseAuthWithGoogle(account);
            } else {
                // Google Sign In failed, update UI appropriately
                // ...
            }
        }
    }
	
	private void firebaseAuthWithGoogle(GoogleSignInAccount acct) {
        Log.d(TAG, "firebaseAuthWithGoogle:" + acct.getId());

        AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);
        autenticacao.signInWithCredential(credential)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete( Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            
                            FirebaseUser user = autenticacao.getCurrentUser();
                            
                        } else {
                            // If sign in fails, display a message to the user.
                        	System.out.println("not passed");
                        }

                        // ...
                    }
                });
    }

	
	public void ReAuthUser() {
		usuarioFirebase = FirebaseAuth.getInstance().getCurrentUser();
		
		AuthCredential credencial = EmailAuthProvider.getCredential("email", "senha");
		
		usuarioFirebase.reauthenticate(credencial)
			.addOnCompleteListener(new OnCompleteListener<Void>() {
				
				@Override
				public void onComplete(Task<Void> task) {
					if(task.isSuccessful()) {
						System.out.println("Passed");
					}
					
				}
			});
	}
	
	public void signOut() {
		FirebaseAuth.getInstance().signOut();
	}
	
	public void updateUsuario() {
		usuarioFirebase = FirebaseAuth.getInstance().getCurrentUser();
		
		UserProfileChangeRequest profileUpdates = new UserProfileChangeRequest.Builder()
				.setDisplayName("")
				.setPhotoUri(Uri.parse(""))
				.build();
		usuarioFirebase.updateProfile(profileUpdates)
			.addOnCompleteListener(new OnCompleteListener<Void>() {

				@Override
				public void onComplete(Task<Void> task) {
					if(task.isSuccessful()) {
						System.out.println("Passed");
					}
					
				}
			});
		
	}
	
	public void switchPswd() {
		
		usuarioFirebase = FirebaseAuth.getInstance().getCurrentUser();
		
		String pswd = "123";
		
		usuarioFirebase.updatePassword(pswd)
			.addOnCompleteListener(new OnCompleteListener<Void>() {

				@Override
				public void onComplete(Task<Void> task) {
					if(task.isSuccessful()) {
						System.out.println("passed");
					}
					
				}
			});
		
	}
	
	public void sendEmailRedefinitionPswd() {
		String email = "joao.junior.11411@gmail.com";
		
		autenticacao.sendPasswordResetEmail(email)
			.addOnCompleteListener(new OnCompleteListener<Void>() {

				@Override
				public void onComplete(Task<Void> task) {
					if(task.isSuccessful()) {
						System.out.println("passed");
					}
					
				}
			});
	}
	
	public void sendVerificationEmail() {
		usuarioFirebase = FirebaseAuth.getInstance().getCurrentUser();
		
		usuarioFirebase.sendEmailVerification()
			.addOnCompleteListener(new OnCompleteListener<Void>() {

				@Override
				public void onComplete(Task<Void> task) {
					if(task.isSuccessful()) {
						System.out.println("passed");
					}
					
				}
			});;
	}
	
	public void deleteUser() {
		usuarioFirebase = FirebaseAuth.getInstance().getCurrentUser();
		
		usuarioFirebase.delete()
			.addOnCompleteListener(new OnCompleteListener<Void>() {

				@Override
				public void onComplete(Task<Void> task) {
					if(task.isSuccessful()) {
						System.out.println("Deleted");
					}
					
				}
			});
	}
	
	public void setUsuario(FirebaseUser user) {
		user = FirebaseAuth.getInstance().getCurrentUser();
		
		if(user!= null) {
			for(UserInfo profile : user.getProviderData()) {
				String provideId = profile.getProviderId();
				
				String uid = profile.getUid();
				
				String name = profile.getDisplayName();
				
				String email = profile.getEmail();
				
				Uri photo = profile.getPhotoUrl();
			}
		}
	}
}
