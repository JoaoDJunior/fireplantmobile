package com.joaodjunior.auth;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuth.AuthStateListener;
import com.google.firebase.auth.FirebaseUser;

public class UserAuth {
	
	private FirebaseAuth autenticacao;
	
	private FirebaseAuth.AuthStateListener autenticacaoListener;
	
	public void loginAuth() {
		autenticacao = FirebaseAuth.getInstance();
		
		autenticacaoListener = new AuthStateListener() {
			
			@Override
			public void onAuthStateChanged(FirebaseAuth firebaseAuth) {
				FirebaseUser user = firebaseAuth.getCurrentUser();
				if(user != null) {
					
				} else {
					
				}
				
			}
		};
	}
	public void onStart() {
	    autenticacao.addAuthStateListener(autenticacaoListener);
	}
	
	public void onStop() {
	    if (autenticacaoListener != null) {
	        autenticacao.removeAuthStateListener(autenticacaoListener);
	    }
	}
}
